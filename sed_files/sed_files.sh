#!/bin/bash

file=$1
out=$2
in=$3

for folder in */; do
  cd "$folder"
  sed -r "s/$out/$in/g" $file > tmp_$file
  rm $file
  mv tmp_$file $file
  cd ..
done