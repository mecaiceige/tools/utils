# sed_files.sh

- folder
  - subfolder_1
    - run.oar
    - ...
  - subfolder_2
    - run.oar
    - ...
  - subfolder_3
    - run.oar
    - ...
  - subfolder_4
    - ...

```
file = run.oar
char_out = toto
char_out = tata
./sed_files.sh $file $char_out char_in
```

It will change "toto" by "tata" in `run.oar` files in subfolder 1,2 and 3.